FROM eclipse-temurin:17-jre

WORKDIR /
COPY target/twitch-auth-mgr.jar .

CMD ["java","-jar","twitch-auth-mgr.jar"]