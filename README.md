# Twitch Authentication Manager

Manages Twitch Authentication information that is stored in Kubernetes Secret objects.  
At the moment, it only manages Client Credentials provided as ID/Secret pairs by Twitch. If credentials belonging to
Mock APIs are recognized, the program can make sure that the credentials are always kept up to date (
argument `--update-mock-api-auth`).  
Note that at the moment, the program expects all secrets to be placed in the `twitch-auth` namespace.  
The program only performs its task once and then exits, it is meant to be run as a cronjob or similar.

Sample Kubernetes Secret manifest:

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: my-twitch-auth
  annotations:
    winterberry/twitch-auth-mgr: client-credentials
data:
  clientId: aG9mNWd3eDBzdTZvd2ZueXMweWFuOWM4N3pyNnQK 
  clientSecret: NDF2cGRqaTRlOWdpZjI5bWQwb3VldDZma3RkMgo= 
  oauthUrl: bXktdHdpdGNoLWFwaS5jbHVzdGVyLmxvY2FsCg==
```

## TODO's

- If credentials relating to the real Twitch API are recognized, the program should be able to check if they are
  accepted by Twitch.
- The program should be able to manage OAuth2 Tokens distributed by Twitch. Access Tokens for project accounts (e.g.
  Chatbots) should be Kubernetes Secrets, tokens for end users should be kept in a database.  
  This can be done as an extention of the
  existing [Credential Manager](https://github.com/PhilippHeuer/credential-manager) library.
