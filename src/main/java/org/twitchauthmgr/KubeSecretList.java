package org.twitchauthmgr;

import io.kubernetes.client.openapi.ApiClient;
import io.kubernetes.client.openapi.ApiException;
import io.kubernetes.client.openapi.Configuration;
import io.kubernetes.client.openapi.apis.CoreV1Api;
import io.kubernetes.client.openapi.models.V1Secret;
import io.kubernetes.client.util.Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * List objects that can be created by importing authorization data from Kubernetes Secret objects.
 * <br>
 * We specify that a kubernetes secret relates to our project by placing an annotation such as:
 *
 * <pre>
 * apiVersion: v1
 * kind: Secret
 * metadata:
 *   name: my-secret
 *   annotations:
 *     winterberry/twitch-auth-mgr: client-credentials
 * data:
 *   ...
 * </pre>
 */
public final class KubeSecretList extends ArrayList<V1Secret> {

  /** Time in seconds after which calls to the Kubernetes API time out. */
  public static final int KUBE_TIMEOUT_SECONDS = 5;

  /**
   * Key that, if provided in the Kubernetes annotations, indicates that a Secret Object is to be
   * recognized by this program.
   */
  public static final String ANNOTATION_KEY = "winterberry/twitch-auth-mgr";

  private static final Logger logger = LoggerFactory.getLogger(KubeSecretList.class);

  /**
   * Load and parse Kubenretes Secrets that correspond to Client Credentials. They are recognized by
   * their Kubernetes annotation
   *
   * <pre>winterberry/twitch-auth-mgr: client-credentials</pre>
   *
   * .
   *
   * @param namespace Namespace in which we search for Secret objects.
   * @return Kubernetes Object parsed as V1Secret.
   */
  public static KubeSecretList loadClientCredentials(String namespace)
      throws IOException, ApiException {
    var clientCredentials = loadSecrets(namespace, "client-credentials");
    logger.info(
        "Loading following secrets as client credentials: " + getSecretNames(clientCredentials));
    return clientCredentials;
  }

  /**
   * TODO: This function is meant to parse OAuth2 tokens that correspond to our chat bots.
   *
   * @param namespace Namespace in which we search for Secret objects.
   * @return Kubernetes Object parsed as V1Secret.
   */
  public static KubeSecretList loadChatToken(String namespace) throws IOException, ApiException {
    return loadSecrets(namespace, "chat-access-token");
  }

  /**
   * Get access to the Kubernetes API based on the local Kubeconfig file or on the configured
   * service account
   *
   * @return Access to the Kubernetes API
   * @throws IOException If an I/O error occurrs during Client creation.
   */
  public static CoreV1Api getKubeApi() throws IOException {
    ApiClient client = Config.defaultClient();
    Configuration.setDefaultApiClient(client);

    return new CoreV1Api();
  }

  private static KubeSecretList loadSecrets(String namespace, String annotationValue)
      throws IOException, ApiException {

    var coreApi = getKubeApi();

    // List secrets in the specified namespace
    List<V1Secret> secrets =
        coreApi
            .listNamespacedSecret(
                namespace,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                KUBE_TIMEOUT_SECONDS,
                false)
            .getItems();

    // Filter secrets based on the annotation key-value pair
    KubeSecretList filteredSecrets = new KubeSecretList();
    for (V1Secret secret : secrets) {
      if (secret.getMetadata() != null && secret.getMetadata().getAnnotations() != null) {
        String value = secret.getMetadata().getAnnotations().get(ANNOTATION_KEY);
        if (value != null && value.equals(annotationValue)) {
          filteredSecrets.add(secret);
        }
      }
    }

    return filteredSecrets;
  }

  /** Print a list of the names of loaded secrets for logging */
  private static String getSecretNames(List<V1Secret> secretList) {
    List<String> secretNames = new ArrayList<>();
    for (var secret : secretList) {
      if (secret.getMetadata() != null) {
        secretNames.add(secret.getMetadata().getName());
      } else {
        secretNames.add("[unnamed secret]");
      }
    }
    return String.join(",", secretNames);
  }
}
