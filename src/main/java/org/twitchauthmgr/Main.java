package org.twitchauthmgr;

import io.kubernetes.client.openapi.models.V1Secret;
import io.kubernetes.client.openapi.ApiException;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Properties;

import static java.net.HttpURLConnection.HTTP_BAD_REQUEST;
import static java.net.HttpURLConnection.HTTP_OK;

public final class Main {

  static final String KUBE_NAMESPACE = "twitch-auth";
  static final String OPTION_UPDATE_MOCK_AUTH = "update-mock-api-auth";
  static final String OPTION_VERSION = "version";

  private static final Logger logger = LoggerFactory.getLogger(Main.class);

  public static void main(String[] args)
      throws ParseException, IOException, ApiException, InterruptedException {
    Options options = new Options();
    options.addOption(
        Option.builder()
            .longOpt(OPTION_UPDATE_MOCK_AUTH)
            .desc("Check and Update Mock API authentications.")
            .build());
    options.addOption(
        Option.builder().longOpt("version").desc("Print the version of the program").build());

    var commands = new DefaultParser().parse(options, args);

    if (commands.hasOption(OPTION_UPDATE_MOCK_AUTH)) {
      logger.info(
          "Provided Option '--"
              + OPTION_UPDATE_MOCK_AUTH
              + "'. Mock API authentications will be checked and updated");
      updateAllMockAuth();
    } else if (commands.hasOption(OPTION_VERSION)) {
      printVersion();
    }
  }

  /** Print program information based on the data provided in the POM file. */
  private static void printVersion() {
    try (InputStream inputStream =
        Main.class.getClassLoader().getResourceAsStream("programInfo.properties")) {
      Properties properties = new Properties();
      properties.load(inputStream);
      String version = properties.getProperty("version");
      String programName = properties.getProperty("artifactId");
      System.out.println(programName + " version " + version);
    } catch (IOException e) {
      throw new RuntimeException("Error loading programInfo.properties file", e);
    }
  }

  /**
   * Checks if any of the authentication info to Mock APIs needs to be updated. Updates Kubernetes
   * secret to reflect new authentication data, if necessary. All API Credentials with an oauthURL
   * other than "id.twitch.tv/..." are assumed to link to a Mock API.
   */
  public static void updateAllMockAuth() throws IOException, ApiException, InterruptedException {

    var clientCredentialSecrets = KubeSecretList.loadClientCredentials(KUBE_NAMESPACE);
    var mockApiCredentialSecrets = filterMockApiCredentials(clientCredentialSecrets);

    for (var credentialSecret : mockApiCredentialSecrets) {
      checkOrUpdateMockApiSecret(credentialSecret);
    }
  }

  /**
   * Checks if the data stored in a Kubernetes Secret still authenticates to a Mock API. Updates the
   * data if necessary.
   *
   * @param credentialSecret Secret relating to a Mock API credential set.
   * @throws IOException When a secret is malformed.
   * @throws ApiException If Kubernetes access does not work as intended.
   */
  private static void checkOrUpdateMockApiSecret(V1Secret credentialSecret)
      throws IOException, ApiException, InterruptedException {

    var kubeApi = KubeSecretList.getKubeApi();
    var credential = TwitchClientCredential.fromSecret(credentialSecret);
    var loginResponseCode = credential.attemptAuthentication();
    var mockApiUrl = credential.getAuthServer();

    if (loginResponseCode == HTTP_OK) {
      logger.info("Successfully logged into Mock API under " + mockApiUrl);
    } else if (loginResponseCode == HTTP_BAD_REQUEST) {
      var newCredential = TwitchClientCredential.getFromMockServer(mockApiUrl);
      var secretMetadata = credentialSecret.getMetadata();

      String secretName;
      if (secretMetadata != null) {
        secretName = secretMetadata.getName();
      } else {
        logger.error("No metadata found for secret " + credentialSecret);
        return;
      }

      kubeApi.replaceNamespacedSecret(
          secretName, KUBE_NAMESPACE, newCredential.toSecret(secretName), null, null, null, null);

      logger.info("Updated credentials for Mock API under " + mockApiUrl);
    } else {
      logger.info(
          "Received unexpected Response Code "
              + loginResponseCode
              + " when trying to log into Mock API with credentials "
              + credential);
    }
  }

  /**
   * From a list of Client Credential Secrets, remove all but those credentials that refer to Mock
   * API credentials.
   *
   * @param clientCredentialSecrets List of Client Credential Secrets
   * @return List of Client Credential Secrets that all refer to Mock API credentials.
   */
  private static List<V1Secret> filterMockApiCredentials(List<V1Secret> clientCredentialSecrets) {
    List<V1Secret> filteredSecrets = new ArrayList<>();

    for (V1Secret secret : clientCredentialSecrets) {
      if (secret.getData() != null) {
        byte[] oauthUrlAscii = secret.getData().get("oauthUrl");
        String oauthUrl = new String(oauthUrlAscii, StandardCharsets.US_ASCII);
        if (oauthUrl.startsWith("https://id.twitch.tv")) {
          filteredSecrets.add(secret);
        }
      }
    }

    return filteredSecrets;
  }

  private Main() {
    throw new IllegalStateException("Utility Class");
  }
}
