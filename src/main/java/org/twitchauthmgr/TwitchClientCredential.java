package org.twitchauthmgr;

import com.jayway.jsonpath.JsonPath;
import io.kubernetes.client.openapi.models.V1ObjectMeta;
import io.kubernetes.client.openapi.models.V1Secret;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

import lombok.Data;
import org.apache.commons.codec.binary.Base64;

/**
 * Credentials that authorize an application with the Twitch API. They consist of the ClientID and
 * the ClientSecret, and can be used to obtain App Access Tokens or OAuth Tokens. Not to be mistaken
 * with the tokens themselves. See: <a
 * href="https://dev.twitch.tv/docs/authentication/register-app/">Registering your App</a> Besides
 * the ID and Secret, with each credential we specify the URL of the domain server. Usually, this is
 * <a href="https://id.twitch.tv/oauth2">https://id.twitch.tv/oauth2</a>, but alternatively,
 * addresses for Mock APIs can be provided.
 */
@Data
public class TwitchClientCredential {
  private final String clientId;
  private final String clientSecret;
  private final String authServer;

  /**
   * Makes a request to a Mock Server and asks it for the ClientID and ClientSecret information.
   *
   * @param mockServerUrl Path to the Twitch API mock server.
   * @return A new TwitchClientCredential object based on the obtained information.
   * @throws IOException If the HTTP request to the API Server failed.
   * @throws InterruptedException If the thread performing the request gets interrupted.
   */
  public static TwitchClientCredential getFromMockServer(String mockServerUrl)
      throws IOException, InterruptedException {
    String apiUrl = mockServerUrl + "/units/clients";
    String response = httpGetRequestBody(apiUrl);

    String clientId = extractValueFromJson(response, "$.data[0].ID");
    String clientSecret = extractValueFromJson(response, "$.data[0].Secret");

    String authUrl = mockServerUrl + "/auth";

    return new TwitchClientCredential(clientId, clientSecret, authUrl);
  }

  /**
   * Create a new TwitchClientCredential object based on a Kubernetes Secret.
   *
   * @param secret The Kubernetes secret.
   * @return A new TwitchClientCredential object.
   * @throws IOException If the Kubernetes secret does not contain the expected information or is
   *     malformed.
   */
  public static TwitchClientCredential fromSecret(V1Secret secret) throws IOException {
    try {

      String clientId, clientSecret, authUrl;
      if (secret.getData() != null) {
        clientId = decodeAscii(secret.getData().get("clientId"));
        clientSecret = decodeAscii(secret.getData().get("clientSecret"));
        authUrl = decodeAscii(secret.getData().get("oauthUrl"));
      } else {
        throw new IOException("The secret is missing the data field");
      }

      return new TwitchClientCredential(clientId, clientSecret, authUrl);
    } catch (NullPointerException e) {
      throw new IOException(
          "The secret did not have the expected fields clientId, clientSecret, oauthUrl");
    }
  }

  /**
   * Create a Kubernetes secret object that contains the credentials.
   *
   * @param secretName The desired name of the Kubernetes secret.
   * @return The Kubernetes secret object.
   */
  public V1Secret toSecret(String secretName) {
    V1Secret secret = new V1Secret();
    secret.setMetadata(new V1ObjectMeta().name(secretName));

    secret.setData(
        Map.of(
            "clientId", this.getClientId().getBytes(),
            "clientSecret", this.getClientSecret().getBytes(),
            "oauthUrl", this.getAuthServer().getBytes()));

    return secret;
  }

  /**
   * Try to perform an authentication request to an API server. This method is used to check if the
   * API credentials are accurate.
   *
   * @return The HTTP status code of the login attempt.
   * @throws IOException If an I/O error occurrs during sending or receiving.
   * @throws InterruptedException If the thread that performs the request gets interrupted.
   */
  public int attemptAuthentication() throws IOException, InterruptedException {
    String requestUrl =
        this.authServer
            + "/token"
            + "?client_id="
            + this.clientId
            + "&client_secret="
            + this.clientSecret
            + "&grant_type=client_credentials";

    var response = sendPostRequest(requestUrl);

    return response.statusCode();
  }

  private static String decodeAscii(byte[] asciiString) {
    return new String(asciiString, StandardCharsets.US_ASCII);
  }

  private HttpResponse<Void> sendPostRequest(String urlString)
      throws IOException, InterruptedException {
    var client = HttpClient.newHttpClient();
    var request =
        HttpRequest.newBuilder()
            .uri(URI.create(urlString))
            .POST(HttpRequest.BodyPublishers.noBody())
            .build();

    // Send the request and get the response
    return client.send(request, HttpResponse.BodyHandlers.discarding());
  }

  /**
   * Send a GET request to a URL and return its response body.
   *
   * @param url Address to which the request is sent.
   * @return Response to the GET request.
   * @throws IOException If the request failed. (Return code other than 200)
   * @throws InterruptedException If the operation is interrupted before finishing.
   */
  private static String httpGetRequestBody(String url) throws IOException, InterruptedException {
    var client = HttpClient.newHttpClient();
    var request = HttpRequest.newBuilder().uri(URI.create(url)).GET().build();
    var response = client.send(request, HttpResponse.BodyHandlers.ofString());

    int responseCode = response.statusCode();

    if (responseCode == HttpURLConnection.HTTP_OK) {
      return response.body();
    } else {
      throw new IOException("GET request failed with HTTP code: " + responseCode);
    }
  }

  private static String extractValueFromJson(String jsonString, String jsonPath) {
    return JsonPath.parse(jsonString).read(jsonPath);
  }
}
