import io.kubernetes.client.openapi.ApiException;
import io.kubernetes.client.openapi.apis.CoreV1Api;
import io.kubernetes.client.openapi.models.V1Namespace;
import io.kubernetes.client.openapi.models.V1ObjectMeta;
import io.kubernetes.client.openapi.models.V1Secret;
import org.twitchauthmgr.KubeSecretList;
import org.junit.Test;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.*;

import static junit.framework.TestCase.assertEquals;

public class KubeSecretListTest {
  /**
   * Try to load Client Credentials that are stored in a Kubernetes cluster. This test actually
   * creates a scratch namespace and secrets, therefore access permissions are required.
   * If this test crashes prematurely, it might result in left-over namespaces.
   */
  @Test
  public void loadClientCredFromKubernetes() throws IOException, ApiException {
    final String testNamespace = generateRandomNamespace();

    var coreApi = KubeSecretList.getKubeApi();

    try {
    // setup test data
    createNamespace(coreApi, testNamespace);

    createSecret(
        coreApi,
        testNamespace,
        "client-cred",
        Collections.singletonMap("winterberry/twitch-auth-mgr", "client-credentials"),
        Map.of(
            "clientId",
            ascii("abcd"),
            "clientSecret",
            ascii("efgh"),
            "oauthUrl",
            ascii("http://localhost")));

    createSecret(
        coreApi, testNamespace, "distractor", null, Map.of("randomdata", ascii("nothing")));

    // load secrets through our function
    var secrets = KubeSecretList.loadClientCredentials(testNamespace);

    String secretName = "";
    if (secrets.size() > 0) {
      secretName = Objects.requireNonNull(secrets.get(0).getMetadata()).getName();
    }

      assertEquals(1, secrets.size());
      assertEquals("client-cred", secretName);
    } finally {
      deleteNamespace(coreApi, testNamespace);
    }
  }

  private static void createNamespace(CoreV1Api coreV1Api, String namespaceName)
      throws ApiException {
    V1Namespace namespace = new V1Namespace();
    namespace.setMetadata(new V1ObjectMeta().name(namespaceName));

    coreV1Api.createNamespace(namespace, null, null, null, null);
    System.out.println("Namespace created: " + namespaceName);
  }

  private static void createSecret(
      CoreV1Api coreV1Api,
      String namespaceName,
      String secretName,
      Map<String, String> annotations,
      Map<String, byte[]> data)
      throws ApiException {
    V1Secret secret = new V1Secret();
    secret.setMetadata(
        new V1ObjectMeta().name(secretName).annotations(annotations).namespace(namespaceName));
    secret.setData(data);

    coreV1Api.createNamespacedSecret(namespaceName, secret, null, null, null, null);
    System.out.println("Secret created: " + secretName);
  }

  private static void deleteNamespace(CoreV1Api coreV1Api, String namespaceName)
      throws ApiException {
    coreV1Api.deleteNamespace(namespaceName, null, null, null, null, "Foreground", null);
    System.out.println("Namespace deleted: " + namespaceName);
  }

  private byte[] ascii(String s) {
    return s.getBytes(StandardCharsets.US_ASCII);
  }

  private static String generateRandomNamespace() {
    Random random = new Random();
    int randomNumber =
        random.nextInt(
            0xFFFFFF); // Generate a random number between 0 and 16777215 (FFFFFF in hexadecimal)
    return String.format(
        "scratch-%06x", randomNumber); // Format the number as a 6-digit hexadecimal string
  }
}
