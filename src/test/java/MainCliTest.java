import io.kubernetes.client.openapi.ApiException;
import org.apache.commons.cli.ParseException;
import org.junit.Test;
import org.twitchauthmgr.Main;
import java.io.IOException;

public class MainCliTest {
  @Test
  public void VersionTest() throws IOException, InterruptedException, ApiException, ParseException {
    String[] args = {"--version"};
    Main.main(args);
  }

  @Test
  public void UpdateMockApiAuthTest()
      throws IOException, InterruptedException, ApiException, ParseException {
    String[] args = {"--update-mock-api-auth"};
    Main.main(args);
  }
}
