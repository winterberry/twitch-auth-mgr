import io.kubernetes.client.openapi.models.V1ObjectMeta;
import io.kubernetes.client.openapi.models.V1Secret;
import org.twitchauthmgr.TwitchClientCredential;
import org.junit.Test;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.nio.charset.StandardCharsets;
import java.util.Map;

import static junit.framework.TestCase.assertEquals;

public class TwitchClientCredentialTest {

  /** Try to parse a Kuberntes secret into a TwitchClientCredential object */
  @Test
  public void parseFromSecret() throws IOException {
    V1Secret secret = new V1Secret();
    secret.setMetadata(new V1ObjectMeta().name("test-secret").namespace("default"));
    secret.setData(
        Map.of(
            "clientId",
            ascii("asdf"),
            "clientSecret",
            ascii("zxdc"),
            "oauthUrl",
            ascii("http://localhost")));

    TwitchClientCredential credential = TwitchClientCredential.fromSecret(secret);

    TwitchClientCredential expected_credential =
        new TwitchClientCredential("asdf", "zxdc", "http://localhost");

    assertEquals(credential, expected_credential);
  }

  /** Log into a mock API server with what we know to be good credentials. */
  @Test
  public void loginWithGoodCredentials() throws IOException, InterruptedException {
    TwitchClientCredential goodCredentials =
        TwitchClientCredential.getFromMockServer("http://twitch-mock-api.snowpoke.ink");
    int responseCode = goodCredentials.attemptAuthentication();

    assertEquals(HttpURLConnection.HTTP_OK, responseCode);
  }

  /** Try to log into the Twitch API with bad credentials. */
  @Test
  public void loginWithBadCredentials() throws IOException, InterruptedException {
    TwitchClientCredential badCredentials =
        new TwitchClientCredential("bad_id", "bad_secret", "https://id.twitch.tv/oauth2");
    int responseCode = badCredentials.attemptAuthentication();

    assertEquals(HttpURLConnection.HTTP_BAD_REQUEST, responseCode);
  }

  private byte[] ascii(String s) {
    return s.getBytes(StandardCharsets.US_ASCII);
  }
}
